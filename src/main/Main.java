package main;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
		System.out.println("Informe um valor multiplo de 10, entre 100 e 1000!");
		int entrada = scan.nextInt();
		if(entrada > 100 && entrada < 1000 && entrada%10 == 0) {
			int[] lista = new int[10];
			for(int i = 1; i < 11; i++) {
				if(i % 3 == 0) {
					lista[i-1] = (int) (i * 0.3 * entrada);
				}else {
					lista[i-1] = (int) (i * 0.1 * entrada);
				}
			}
			System.out.println("Deseja saber o somat�rio das posi��es �mpares ou pares do array? \n(responder 'i' para impar e 'p' para pares)");
			String resposta= scan.next();
			int soma = 0;
			if(!resposta.equals("i") || resposta.equals("p")) {
				System.out.println("Op��o Incorreta!");
			}
			for(int i = 1; i < 11; i++) {
				if("i".equals(resposta) && i%2 != 0) {
					soma += lista[i-1];
				}else
				if("p".equals(resposta) && i%2 == 0){
					soma += lista[i-1];
				}
			}
			System.out.println(soma);
		}else {
			System.out.println("N�mero informado n�o atende aos requisitos!");
		}}catch (Exception e) {
			System.out.println("N�mero informado incorretamente!");
		}
	}

}
